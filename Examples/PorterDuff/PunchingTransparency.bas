Rem Basic! program.   PunchingTransparency.bas
! Modified Brochi's Punching Code with PortDuff blending
FN.DEF               maskBmp(bmp, stepSize, mr,mg,mb)
 GR.BITMAP.SIZE      bmp,x,y
 GR.COLOR            0,0,0,0
 FOR i             = 0 TO x-1 STEP stepSize
  FOR j            = 0 TO y-1 STEP stepSize
   GR.GET.BMPIXEL    bmp,i,j,a,r,g,b
   IF                r=mr & g=mg & b=mb THEN GR.BITMAP.FILL bmp, i,j
  NEXT 
  GR.RENDER 
 NEXT
FN.END


GR.OPEN             250,20,30,150,0,1


dy = 25
nr = 70
FOR i=0 TO nr
 GR.COLOR           255, 255/nr*i, 30, 60 ,1
 GR.RECT            nn,  0,i*dy,1200, i*dy+dy-3
 GR.RENDER
NEXT



GR.BITMAP.CREATE    mask,500,500
GR.BITMAP.DRAW      tmp,mask,100,400

mr                = 244
mg                = 246
mb                = 10

GR.BITMAP.DRAWINTO.START mask
GR.COLOR            255, 20,30,60,1, ,-1 % ,-1 is new (default)
GR.RECT             nn,  0,0,500,500

GR.COLOR            190, mr,mg,mb,1, ,0 % ,0 is new (Clear)

GR.TEXT.SIZE        160
GR.TEXT.BOLD        1
!GR.SET.ANTIALIAS    0 % Not more needed
GR.TEXT.DRAW        nn, 10,150 ,"BASIC!"
GR.TEXT.DRAW        nn, 200,310, "For"
GR.TEXT.DRAW        nn, 30,490 ,"Ever"
!GR.SET.ANTIALIAS    1 % Not more needed

GR.BITMAP.DRAWINTO.END

GR.RENDER

! CALL                maskBmp(mask,20,mr,mg,mb) % Not more needed

speed             = 0.66
DO
 phi              = TORADIANS(speed*t++)
 GR.MODIFY          tmp,"y" , SIN(phi)*400+400
 GR.MODIFY          tmp,"x" , COS(phi)*100+100
 GR.RENDER
 PAUSE              5
UNTIL 0
