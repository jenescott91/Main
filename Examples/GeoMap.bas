REM Start of BASIC! Include Program CallAnActivity.bas

FN.DEF GetOpenMap(geoLocation$)

 LIST.CREATE S, commandListPointer
 LIST.ADD commandListPointer~
 "new Intent(Intent.ACTION_VIEW);" ~
 "setData(" + CHR$(34) + geoLocation$ + CHR$(34) + ");"  ~
 "resolveActivity(getPackageManager());" ~
 "EOCL"

 BUNDLE.CREATE appVarPointer
 BUNDLE.PL appVarPointer,"***CommandList***",commandListPointer

 APP.SAR appVarPointer


 FN.RTN appVarPointer
FN.END
DEBUG.OFF

myGeoLocation$ = "geo:0,0?q=34.99,-106.61(Treasure)"
GetOpenMap(myGeoLocation$)

END

!!
Data URI Scheme
geo:latitude,longitude
Show the map at the given longitude and latitude.
Example: "geo:47.6,-122.3"

geo:latitude,longitude?z=zoom
Show the map at the given longitude and latitude at a certain zoom level. 
A zoom level of 1 shows the whole Earth, centered at the given lat,lng. 
The highest (closest) zoom level is 23.
Example: "geo:47.6,-122.3?z=11"

geo:0,0?q=lat,lng(label)
Show the map at the given longitude and latitude with a string label.
Example: "geo:0,0?q=34.99,-106.61(Treasure)"

geo:0,0?q=my+street+address
Show the location for "my street address" (may be a specific address or location query).
Example: "geo:0,0?q=1600+Amphitheatre+Parkway%2C+CA"

Note: All strings passed in the geo URI must be encoded. 
For example, the string 1st & Pike, Seattle should become 1st%20%26%20Pike%2C%20Seattle. 
Spaces in the string can be encoded with %20 or replaced with the plus sign (+).

!!
